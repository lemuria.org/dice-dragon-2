module gitlab.com/lemuria.org/dice-dragon-2

go 1.18

require (
	github.com/bwmarrin/discordgo v0.25.0
	github.com/knadh/koanf v1.4.1
)

require (
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/mitchellh/copystructure v1.2.0 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/mitchellh/reflectwalk v1.0.2 // indirect
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b // indirect
	golang.org/x/sys v0.0.0-20220422013727-9388b58f7150 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
