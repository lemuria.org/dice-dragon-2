package main

import (
    "fmt"
    "os"
    "os/signal"
    "syscall"
    "log"

	"github.com/knadh/koanf"
	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/file"
    "github.com/bwmarrin/discordgo"
)

// Global koanf instance. Use "." as the key path delimiter. This can be "/" or any character.
var config = koanf.New(".")


func main() {
    // load config
	if err := config.Load(file.Provider("config.yaml"), yaml.Parser()); err != nil {
		log.Fatalf("error loading config: %v", err)
	}
    // load and merge the token from a different config file that's not in git
    if err := config.Load(file.Provider("token.yaml"), yaml.Parser()); err != nil {
		log.Fatalf("error loading config: %v", err)
	}

    // Create a new Discord session using the provided bot token.
    dg, err := discordgo.New("Bot " + config.String("token"))
    if err != nil {
        fmt.Println("error creating Discord session,", err)
        return
    }

    // Register the messageCreate func as a callback for MessageCreate events.
    dg.AddHandler(messageCreate)
    dg.AddHandler(messageUpdate)

    // In this example, we only care about receiving message events.
    dg.Identify.Intents = discordgo.IntentsGuildMessages | discordgo.IntentsDirectMessages

    // Open a websocket connection to Discord and begin listening.
    err = dg.Open()
    if err != nil {
        fmt.Println("error opening connection,", err)
        return
    }

    // Wait here until CTRL-C or other term signal is received.
    fmt.Println("Bot is now running. Press CTRL-C to exit.")
    sc := make(chan os.Signal, 1)
    signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
    <-sc

    // Cleanly close down the Discord session.
    dg.Close()
}
