package main

import (
    "fmt"

    "github.com/bwmarrin/discordgo"
)

// This function will be called (due to AddHandler in main.go) every time a new
// message is created on any channel that the authenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

    // Ignore all messages created by the bot itself
    if m.Author.ID == s.State.User.ID {
        return
    }

    fmt.Println("received message: " + m.Content)

    // If the message is "ping" reply with "Pong!"
	if m.Content == "ping" {
		s.ChannelMessageSend(m.ChannelID, "Pong!")
	}

	// If the message is "pong" reply with "Ping!"
	if m.Content == "pong" {
		s.ChannelMessageSend(m.ChannelID, "Ping!")
	}
}


func messageUpdate(s *discordgo.Session, m *discordgo.MessageUpdate) {

    // Ignore all messages created by the bot itself
    if m.Author.ID == s.State.User.ID {
        return
    }

    fmt.Println("received update: " + m.Content)
}
